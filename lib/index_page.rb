class IndexPage
  def initialize(driver)
    @driver = driver
  end

  def click_link(link)
    @driver.find_element(:xpath, "//span[text()='#{link}']").click

    SearchResultsPage.new(@driver)
  end

  def verify_page
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    wait.until { @driver.find_element(:id => 'pagecontainer') }
  end

  def select_language(language)
    option = Selenium::WebDriver::Support::Select.new(@driver.find_element(:id, 'chlang'))
    option.select_by(:text, language)
  end
end