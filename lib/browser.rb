class Browser
  attr_reader :driver

  def initialize(browser_name)
    @driver = start_browser(browser_name)
    delete_cookies
  end

  def delete_cookies
    @driver.manage.delete_all_cookies
  end

  def open_page(url)
    @driver.navigate.to(url)

    IndexPage.new(@driver)
  end

  def start_browser(browser)
    Selenium::WebDriver::Chrome::Service.executable_path = File.join(Dir.pwd, 'chromedriver')
    Selenium::WebDriver.for :chrome
  end
end