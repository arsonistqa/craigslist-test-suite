class SearchResultsPage

  require 'csv'

  def initialize(driver)
    @driver = driver
  end

  def verify_page
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    wait.until { @driver.find_element(:css => '.search-legend') }
  end

  def get_links_and_save_to_file(link)
    times = if @driver.execute_script("return $('.ban').length") == 1
      @driver.execute_script("return $('.ban').prevAll('p').length")
    else
      @driver.execute_script("return $('.row').length")
    end

    CSV.open("#{link}.csv", 'wb') do |csv|
      @driver.find_elements(:css => '.row .hdrlnk').first(times).each do |row|
        csv << [row.text, row.attribute('href')]
      end
      csv << ['No results found'] if times == 0
    end
  end

end