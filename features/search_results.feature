Feature: Search Results

  Scenario Outline: User is able to perform search on craigslist
    Given I open index page
    When I select 'english' language
    And I click on '<link>' link
    Then I get all data from search results and save to '<link>' file

  Examples:
    | link      |
    | community |
    | classes   |