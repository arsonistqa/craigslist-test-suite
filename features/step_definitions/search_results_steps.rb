Then /^I get all data from search results and save to '(.*)' file$/ do |name|
  @search_results_page.get_links_and_save_to_file(name)
end