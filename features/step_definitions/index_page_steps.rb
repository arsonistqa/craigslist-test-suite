Given /^I open index page$/ do
  @index_page = @browser.open_page('http://rio.craigslist.org')
  @index_page.verify_page
end

When /^I click on '(.*)' link$/ do |link|
  @search_results_page = @index_page.click_link(link)
  @search_results_page.verify_page
end

When /^I select '(.*)' language$/ do |language|
  @index_page.select_language(language)
end